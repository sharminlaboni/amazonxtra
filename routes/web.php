<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

Route::get('admin', function () {
    return view('index');
});
Route::get('/product',[ProductController::class,'index'])->name('product.import');
Route::post('/product/search',[ProductController::class,'product_search'])->name('product.search');
Route::post('/getcategory',[ProductController::class,'getcategory']);

Route::get('index', function () {
    return view('frontend.index');
});
Route::get('blog', function () {
    return view('frontend.blog');
});
Route::get('portfolio', function () {
    return view('frontend.portfolio');
});
Route::get('login', function () {
    return view('frontend.login');
});
Route::get('signup', function () {
    return view('frontend.signup');
});